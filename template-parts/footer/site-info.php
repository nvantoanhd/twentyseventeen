<?php
/**
 * Displays footer site info
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<div class="site-info">
	<?php wp_nav_menu( array( 'theme_location' => 'max_mega_menu_1' ) ); ?>
</div><!-- .site-info -->
